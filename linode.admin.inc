<?php


function linode_manager_overview() {
  $content = menu_get_item('admin/linode');
  $content = system_admin_menu_block($content);
  $output = theme('admin_block_content', $content);
  return $output;

}
/**
 * list, create, delete accounts
 **/
function manage_linode_accounts($callback_arg = '') {
  $op = isset($_POST['op']) ? $_POST['op'] : $callback_arg;
  switch($op) {
    case t('Register new linode manager') :
    case 'create':
      $output = drupal_get_form('new_linode_manager_form');
      break;
    default:
      // delete handler need to be placed here. user.admin.inc line 18
      $output = drupal_get_form('linode_manager_accounts');
  }
  return $output;
}

function linodes_manager_overview($callback_arg = '') {
  $op = $callback_arg;
  switch($op) {
    case 'linodes' :
      $output = linodes_accounts_overview();
      break;
    case 'dns':
      $output = '';
      break;
    default:
      $output = '';
  }
  return $output;
}
function linode_api_fetch_keys() {
  $query = "SELECT id, name, api_key FROM linode_accounts ";
  $result = db_query($query);
  $keys = array();
  while($row = db_fetch_object($result) ) {
    $keys[$row->api_key] = $row->name;
  }
  return $keys;
}
function linode_api_linode_list($api_key) {
  $request['api_key'] = $api_key;
  $request['api_responseFormat'] = 'json';
  $request['api_action'] = 'linode.list';
  $responce = linode_send_request($request, TRUE);
  return $responce;
}
function linodes_accounts_overview() {
  $output = '';
  $header = array(
    'Linode',
    'Status',
    'Ram/Plan',
    'Storage',
    'Transfer',
  );
  $keys = linode_api_fetch_keys();
  $rows = array();
  foreach($keys as $api_key => $name) {
    $linodes = linode_api_linode_list($api_key);
    $rows[] = array(array('data' => $name, 'colspan' => '5')); // Insert account name as a separator
    foreach($linodes->DATA as $linode) {
      //drupal_set_message('<pre>'. print_r($linodes, 1) . '</pre>');
      $row = array();
      $row[] = l($linode->LABEL, 'https://www.linode.com/members/linode/su.cfm?LinodeID='. $linode->LINODEID);
      $row[] = $linode->STATUS? 'Running': 'Down';
      $row[] = 'Linode '. $linode->TOTALRAM;
      $row[] = $linode->TOTALHD/1024 . 'GB';
      $row[] = $linode->TOTALXFER.'GB';
      $rows[] = $row;
    }
  }

  $output = theme('table', $header, $rows);

  return $output;
}

/**
 * @todo: Add "weight" so managers may give sorting to accounts
 **/
function new_linode_manager_form($form_state) {
  //drupal_set_message('<pre>'. print_r(func_get_args(), 1) . '</pre>');

  $form = array();
  $form['name'] = array(
    '#type'   => 'textfield',
    '#title'  => 'Account name',
    '#description' => 'Arbitrary account name to distinguish linode accounts',
    '#default_value' => isset($form_state['values']['name'])? $form_state['values']['name']: '',
  );
  $form['api_key'] = array(
    '#type'   => 'textfield',
    '#title'  => 'API Key',
    '#description' => 'Necessary API Key in order to perform any kind of function. To acquire an API Key follow these steps: 1) login to linode.com, 2)  click on "My Profile", 3)  Scroll down to API Password section and click on "Generate a new API Password"',
    '#default_value' => isset($form_state['values']['api_key'])? $form_state['values']['api_key']: '',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Register new linode manager'),
  );
  return $form;
}
function new_linode_manager_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  $request['api_key'] = $values['api_key'];
  $request['api_responseFormat'] = 'json';
  $request['api_action'] = 'test.echo'; // This is a dummy test
  $request['test'] = 'success';         // we simply request output of a string to verify that API works

  $responce = linode_send_request($request);
  foreach($responce->ERRORARRAY as $index => $error ) { // If we have errors, lets print them all
    form_set_error('api_key', $error->ERRORMESSAGE);
  }
}

function new_linode_manager_form_submit($form, &$form_state) {
  $account = array(
    'name' => $form_state['values']['name'],
    'api_key' => $form_state['values']['api_key'],
  );
  // This will fail if record already exist.
  // However, the only point of failure is "id" which is an auto_increment
  // so in the end this may not fail ever with current schema.
  drupal_write_record('linode_accounts', $account);
  $form_state['redirect'] = 'admin/linode/api-keys';
}
function linode_send_request($request = array(), $debug = FALSE){
  $curl_handler = curl_init();

  curl_setopt($curl_handler, CURLOPT_URL, 'https://api.linode.com/api/');
  curl_setopt($curl_handler, CURLOPT_POST, 1);
  curl_setopt($curl_handler, CURLOPT_POSTFIELDS,$request);
  curl_setopt($curl_handler, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl_handler, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($curl_handler, CURLOPT_USERAGENT, 'PHP Linode/0.1');

  $responce = curl_exec($curl_handler);
  $responce = json_decode($responce);
  if ( $debug ) {
    linode_send_request_debug($responce);
  }
  return $responce;
}
function linode_send_request_debug($responce) {
  foreach($responce->ERRORARRAY as $error ) {
    drupal_set_message($error->ERRORMESSAGE, 'error');
  }
}
/**
 * Lists all existing linode manager accounts
 */
function linode_manager_accounts() {
  $form = array();
  $query = "SELECT id, name, api_key FROM linode_accounts";
  $result = db_query($query);
  $accounts = array();
  while($row = db_fetch_array($result)) {
    $accounts[] = $row;
  }
  $header = array('Id', 'Name', 'API Key');
  $output = theme('table', $header, $accounts);

  $form['accounts'] = array(
    '#type' => 'markup',
    '#value' => $output,
  );
  return $form;
}

